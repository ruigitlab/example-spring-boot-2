package com.ruinotes.postgresruinotes.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "Grade")
@EntityListeners(AuditingEntityListener.class)
//@JsonIgnoreProperties(value = {"releaseDate"},
//allowGetters = true)
public class Grade implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "Book_Quality")
    private String quality;

    @NotBlank
    @Column(name = "Book_Base_Production")
    private Double baseProduction;
    
    @OneToMany(mappedBy = "grade", cascade = CascadeType.ALL)
    private Set<Publisher> publishers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Double getBaseProduction() {
        return baseProduction;
    }

    public void setBaseProduction(Double baseProduction) {
        this.baseProduction = baseProduction;
    }

}