package com.ruinotes.postgresruinotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ruinotes.postgresruinotes.model.Grade;
@Repository
public interface GradeRepository extends JpaRepository<Grade, Long> {

}