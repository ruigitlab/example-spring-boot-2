package com.ruinotes.postgresruinotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ruinotes.postgresruinotes.model.Category;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}