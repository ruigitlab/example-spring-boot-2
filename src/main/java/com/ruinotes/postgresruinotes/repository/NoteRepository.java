package com.ruinotes.postgresruinotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ruinotes.postgresruinotes.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}