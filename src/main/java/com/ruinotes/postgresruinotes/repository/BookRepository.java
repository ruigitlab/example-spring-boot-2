package com.ruinotes.postgresruinotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ruinotes.postgresruinotes.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}