package com.ruinotes.postgresruinotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ruinotes.postgresruinotes.model.Author;
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}