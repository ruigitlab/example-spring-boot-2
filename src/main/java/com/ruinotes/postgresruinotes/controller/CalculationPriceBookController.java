package com.ruinotes.postgresruinotes.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruinotes.postgresruinotes.model.Book;
import com.ruinotes.postgresruinotes.repository.BookRepository;

@RestController
@RequestMapping("/api/price")
public class CalculationPriceBookController {

	@Autowired
	BookRepository bookRepository;
	
	//Calculation Price Books
	@PutMapping("/update_price")
	public HashMap<String, Object> updatePrice(){
	
		List<Book> bookPrice = bookRepository.findAll();
		
		for (Book book : bookPrice) {
//			Double price, bp;
//			bp = book.getPublisher().getGrade().getBaseProduction();
//			price =  bp * 1.5;
			book.setPrice(book.getPublisher().getGrade().getBaseProduction() * 1.5);
			
			bookRepository.save(book);
		}
		
		HashMap<String, Object> messageCalculation = new HashMap<String, Object>();
		messageCalculation.put("Status", 200);
		messageCalculation.put("Message", "Book Price Calculation Is Success");
		
		return messageCalculation;
	}
}
